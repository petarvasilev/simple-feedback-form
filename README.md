# Simple Feedback Form

We provide feedback form for your website or in other words we are feedback
form as a service. This is the code for the live site residing at [https://www.simplefeedbackform.com](https://www.simplefeedbackform.com).

## Requirements

The usual Laravel 6 requirements plus curl and openssl.

## Getting started

Clone repository. Then install dependencies using

    composer install --no-dev
    
and node dependencies using
   
    npm install
    
give write permissions to bootstrap and storage folder using this

    chmod -R 777 storage
  
and

    chmod -R 777 bootstrap
    
copy `.env.example` into `.env` and fill it in.

Run migrations using

    php artisan migrate
    
Point your web server to the `public` folder and you are ready to go.

Remember to configure Supervisor as well in order for the emails to get sent.

## PayPal configuration

You need to login to your PayPal account and go [here](https://developer.paypal.com/developer/applications)
where you can get a client id and secret by creating a Rest App. Also, when on the Rest App
page you need to subscribe your app to PayPal's webhooks, which is done at the bottom of the page.
Also, remember to fill the PAYPAL_WEBHOOK_ID env variable with the id of the webhook you just
created.

Before going live remember to make yourself an admin and go to /paypal/admin/bootstrap to create and 
activate the plan. You can make yourself an admin by making the `role` column on your user in the
`users` table to be `10`.

## Tests

There are some unit tests which you can run using:

    vendor/bin/phpunit

## Deployment Checklist

1. Do `php artisan config:cache`
2. Do `npm run production`
3. Configure supervisor

## License

MIT License

Copyright 2020 [Petar Vasilev](https://www.petarvasilev.com/)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to
do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.