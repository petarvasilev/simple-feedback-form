window.jQuery = require('jquery');
window.$ = window.jQuery;
require('jquery-ui');
require('jquery-ui-monthpicker');
require('popper.js');
require('bootstrap');
require('./libraries/prism.min');

$(function () {
    $('#cookie-settings-link').on('click', function(event) {
        event.preventDefault();
        $('#cookies-consent-popup').modal('show');
    });

    if ($('body').hasClass('functional-cookies-consent-is-not-given')) {
        $('#cookies-consent-popup').modal('show');
    }
});