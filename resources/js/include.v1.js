(function() {
    var WEBSITE_URL = 'https://www.simplefeedbackform.com/';
    var GET_SETTINGS_URL = WEBSITE_URL + 'settings/get';
    var QUESTION_MARK_URL = WEBSITE_URL + 'img/question-mark.png';
    var SUBMIT_URL = WEBSITE_URL + 'submissions';

    var API_KEY = false;
    var BUSY_CLASS = 'sff-busy';

    if ('_SFF_ENV' in window) {
        if (_SFF_ENV === 'development') {
            WEBSITE_URL = 'http://simplefeedback.local/';
            GET_SETTINGS_URL = WEBSITE_URL + 'settings/get';
            QUESTION_MARK_URL = WEBSITE_URL + '/img/question-mark.png';
            SUBMIT_URL = WEBSITE_URL + 'submissions';
        } else if (_SFF_ENV === 'staging') {
            WEBSITE_URL = 'https://simplefeedback.staging.zavoon.com/';
            GET_SETTINGS_URL = WEBSITE_URL + 'settings/get';
            QUESTION_MARK_URL = WEBSITE_URL + 'img/question-mark.png';
            SUBMIT_URL = WEBSITE_URL + 'submissions';
        }
    }

    window.SimpleFeedbackForm = function(apiKey) {
        API_KEY = apiKey;

        init();
    };

    /**
     * Makes sure that bootstrap is called when the page is loaded
     */
    function init() {
        if (document.readyState === 'complete') {
            bootstrap();
        } else {
            window.addEventListener('load', bootstrap)
        }
    }

    /**
     * Bootstraps form
     */
    function bootstrap() {
        makePostRequest(GET_SETTINGS_URL, {'key' : API_KEY}, function(response) {
            var data = JSON.parse(response);

            if (data.status === 'OK') {
                initForm(data.position, data.background_color, data.form_background_color);
            } else {
                if (console) {
                    console.log(data.message);
                }
            }
        });
    }

    /**
     * Initialize form and button
     *
     * @param position
     * @param backgroundColor
     */
    function initForm(position, backgroundColor, formBackgroundColor) {
        addButton(position, backgroundColor);
        addForm(formBackgroundColor);
        addEvents();
    }

    function addEvents() {
        var button = document.getElementById('simplefeedbackform-button');

        button.addEventListener('click', function() {
            showForm();
        });

        var close = document.getElementById('simplefeedbackform-close');

        close.addEventListener('click', hideForm);
        
        var submit = document.getElementById('simplefeedbackform-submit');
        
        submit.addEventListener('click', postSubmission);

        var name = document.getElementById('simplefeedbackform-name');

        name.addEventListener('keyup', function() {
            clearError(name, 'name');
        });

        var email = document.getElementById('simplefeedbackform-email');

        email.addEventListener('keyup', function() {
            clearError(email, 'email');
        });

        var message = document.getElementById('simplefeedbackform-message');

        message.addEventListener('keyup', function() {
            clearError(message, 'message');
        });
    }

    function clearError(element, name) {
        element.setAttribute('aria-invalid', 'false');
        document.getElementById('simplefeedbackform-' + name + '-error').innerHTML = '';
    }
    
    function postSubmission() {
        var submit = document.getElementById('simplefeedbackform-submit');

        if ( ! hasClass(submit, BUSY_CLASS)) {
            addClass(submit, BUSY_CLASS);

            var name = document.getElementById('simplefeedbackform-name');
            var email = document.getElementById('simplefeedbackform-email');
            var message = document.getElementById('simplefeedbackform-message');

            var data = {
                name: name.value,
                email: email.value,
                message: message.value,
                key: API_KEY,
            };

            makePostRequest(SUBMIT_URL, data, function(response) {
                var data = JSON.parse(response);

                if (data.status === 'OK') {
                    var content = document.getElementById('simplefeedbackform-content');
                    content.style.display = 'none';

                    var success = document.getElementById('simplefeedbackform-success');
                    success.style.display = 'block';
                    success.innerHTML = 'Your message was saved.';
                } else if (data.status === 'BAD') {
                    if ('errors' in data) {
                        // iterate over errors
                        for (var name in data.errors) {
                            if (data.errors.hasOwnProperty(name)) {
                                var errors = data.errors[name];

                                var errorElement = false;
                                var inputElement = false;

                                if (name === 'name' || name === 'email' || name === 'message') {
                                    inputElement = document.getElementById('simplefeedbackform-' + name);
                                    errorElement = document.getElementById('simplefeedbackform-' + name + '-error');
                                }

                                if (errorElement) {
                                    errorElement.innerHTML = errors[0];
                                    inputElement.setAttribute('aria-invalid', 'true');

                                }
                            }
                        }
                    } else if ('message' in data) {

                    }
                }

                removeClass(submit, BUSY_CLASS);
            });
        }
    }

    function hasClass(element, className) {
        var classString = element.className;
        var classes = classString.split(' ');

        for (var i = 0; i < classes.length; i++) {
            if (classes[i] === className) {
                return true;
            }
        }

        return false;
    }

    function removeClass(element, className) {
        var classes = element.className.split(' ');

        for (var i = 0; i < classes.length; i++) {
            if (classes[i] === className) {
                classes.splice(i, 1);
            }
        }

        element.className = classes;
    }

    function addClass(element, className) {
        var classString = element.className;
        element.className = classString.concat(" " + className);
    }

    function showForm()
    {
        var form = document.getElementById('simplefeedbackform-form');

        form.setAttribute('aria-hidden', 'false');
        form.style.display = 'block';
    }

    function hideForm() {
        var form = document.getElementById('simplefeedbackform-form');

        form.setAttribute('aria-hidden', 'true');
        form.style.display = 'none';
    }

    function addForm(formBackgroundColor) {
        var formStyle = getFormStyle(formBackgroundColor);
        var form = '<div id="simplefeedbackform-form" role="form" aria-hidden="true" style="' + formStyle + '"><div id="simplefeedbackform-success"></div><div id="simplefeedbackform-close" role="button" aria-label="Close feedback form" style="' + formStyle + '"><span>&times</span></div><div id="simplefeedbackform-content"><input type="text" id="simplefeedbackform-name" aria-invalid="false" class="simplefeedbackform-input" placeholder="Your name"><div id="simplefeedbackform-name-error" class="simplefeedbackform-error"></div><input type="text" id="simplefeedbackform-email" aria-invalid="false" class="simplefeedbackform-input" placeholder="Your email"><div id="simplefeedbackform-email-error" class="simplefeedbackform-error"></div><textarea id="simplefeedbackform-message" aria-invalid="false" class="simplefeedbackform-input" placeholder="Feedback or ask for help"></textarea><div id="simplefeedbackform-message-error" class="simplefeedbackform-error"></div><button id="simplefeedbackform-submit" aria-label="Submit feedback form">Submit<div class="loader"></div></button></div></div>';

        document.body.insertAdjacentHTML('beforeend', form);
    }

    function getFormStyle(formBackgroundColor) {
        var styles = {
            'background-color' : formBackgroundColor + ';',
        };

        return styleFromObject(styles);
    }

    /**
     * Add button to DOM
     *
     * @param position
     * @param backgroundColor
     */
    function addButton(position, backgroundColor) {
        var questionMark = '<img src="' + QUESTION_MARK_URL + '">';
        var buttonStyle = getButtonStyle(position, backgroundColor);
        var button = '<div id="simplefeedbackform-button" role="button" aria-label="Open feedback form" style="' + buttonStyle+ '">' + questionMark + '</div>';

        document.body.insertAdjacentHTML('beforeend', button);
    }

    /**
     * Returns the button style
     *
     * @param position
     * @param backgroundColor
     * @returns {string}
     */
    function getButtonStyle(position, backgroundColor)
    {
        var styles = {
            'background-color' : backgroundColor + ';',
        };

        if (position === 'left bottom') {
            styles['left'] = '-5px;';
            styles['bottom'] = '12px;';
        } else if (position === 'bottom left') {
            styles['left'] = '12px;';
            styles['bottom'] = '-5px;';
        } else if (position === 'bottom center') {
            styles['left'] = '0;';
            styles['right'] = '0;';
            styles['margin-left'] = 'auto;';
            styles['margin-right'] = 'auto;';
            styles['bottom'] = '-5px;';
        } else if (position === 'bottom right') {
            styles['right'] = '12px;';
            styles['bottom'] = '-5px;';
        }  else if (position === 'right bottom') {
            styles['right'] = '-5px;';
            styles['bottom'] = '12px;';
        }

        return styleFromObject(styles);
    }

    function styleFromObject(styles) {
        var style = '';

        for (var name in styles) {
            style += name + ':' + styles[name];
        }

        return style;
    }

    /**
     * Makes an AJAX POST request
     *
     * @param url
     * @param data
     * @param cb
     */
    function makePostRequest(url, data, cb) {
        var http = new XMLHttpRequest();
        var params = '';
        var length = 0;
        var length2 = 0;

        for (var i in data) {
            length++;
        }

        for (var name in data) {
            length2++;
            if (data.hasOwnProperty(name)) {
                params += name + '=' + data[name];

                if (length2 !== length) {
                    params += '&';
                }
            }
        }

        http.open('POST', url, true);

        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        http.onreadystatechange = function() {//Call a function when the state changes.
            if(http.readyState == 4 && http.status == 200) {
                cb(http.responseText);
            }
        };

        http.send(params);
    }
})();