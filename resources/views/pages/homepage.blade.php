@extends('layouts.app')

@section('content')
    <div class="container homepage">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h1>Simple Feedback Form</h1></div>

                    <div class="card-body">
                        <p>
                            Welcome! We provide feedback form for your website. To get started all you need to do is
                            sign up, copy the code shown on your account's dashboard and paste it into your website's
                            code. Note that we have a free plan which has {{ config('plans.free.limit') }} feedback
                            entries a month, if you need more entries you can always upgrade or alternatively you can
                            get the code and host your own version for free and have as many submissions as you want!
                        </p>

                        <p>
                            You can customize the open button & feedback form's background color through your account's
                            settings page. Also, regarding notifications you can choose to get an email every time your
                            feedback form is submitted and you, also, get a notification if only a few submissions are left
                            on your plan (to do with the free plan).
                        </p>

                        <p>
                            Also, our feedback form does not contain any branding. Feel free to add your own branding with
                            JavaScript and CSS.
                        </p>

                        <h2 class="mrgb-20">Our feedback form service is: simple, fast,
                            <a href="https://bitbucket.org/petarvasilev/simple-feedback-form/">open-source</a>.</h2>

                        <p>
                            How it works? You just login/sign up into our website copy the code shown on your
                            dashboard into your website's code and then you'll see a little button with a question mark
                            appear on your website. Click the button and the form will appear.
                        </p>

                        <p>
                            This is how the form looks by default:
                        </p>

                        <div>
                            <img class="max-100" src="{{ asset('/img/form-closer.png') }}" alt="Feedback Form Closer Screenshot">
                        </div>

                        <hr>

                        <p>
                            Here is a wide view of the form:
                        </p>

                        <div>
                            <img class="max-100" src="{{ asset('/img/form-screenshot.png') }}" alt="Feedback Form Screenshot">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
