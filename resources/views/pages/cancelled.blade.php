@extends('layouts.app')

@section('content')
    <div class="container homepage">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h1>Payment not approved</h1></div>

                    <div class="card-body">
                        <p>
                            You were not subscribed because you didn't approve the payment.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
