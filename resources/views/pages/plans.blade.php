@extends('layouts.app')

@section('content')
    <div class="container homepage">
        <div class="row justify-content-center">
            <div class="col col-xs-12 col-sm-12 col-md-8">
                @include('partials.flash')

                <div class="card">
                    <div class="card-header"><h1>{{ __('Our Plans') }}</h1></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="plan">
                                    <div class="title">{{ __('Free') }}</div>
                                    <div class="content">
                                        <div class="line">{{ config('plans.free.limit') . __(' entries/month') }}</div>
                                        <div class="line">{{ __('€0/month') }}</div>
                                    </div>
                                    <div class="action">
                                        <a href="{{ route('register') }}">{{ __('Sign Up') }}</a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="plan">
                                    <div class="title">{{ __('Epic') }}</div>
                                    <div class="content">
                                        <div class="line">{{ __('Unlimited entries') }}</div>
                                        <div class="line">{{ __('€10/month') }}</div>
                                    </div>
                                    <div class="action">
                                        <a href="{{ route('subscribeEpic') }}">{{ __('Subscribe with PayPal') }}</a>
                                    </div>
                                </div>
                            </div>

                            <p id="plans-note" class="centered">Note before subscribing to the paid plan you need to register & login.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
