@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @include('partials.flash')

                <div class="card">
                    <div class="card-header">{{ __('Feedback form settings') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('settingsPost') }}">
                            @csrf

                            @include('partials.input.select', [
                                'text'       => 'Button position *',
                                'identifier' => 'position',
                                'edit'       => $user,
                                'values'     => $positionChoices,
                                'copy'       => '<p class="field-note">Note that you can also use CSS to customize the form look & position.</p>'
                            ])

                            @include('partials.input.default', [
                                'text'       => 'Button background color *',
                                'identifier' => 'background_color',
                                'edit'       => $user,
                                'type'       => 'color'
                            ])

                            @include('partials.input.default', [
                                'text'       => 'Form background color *',
                                'identifier' => 'form_background_color',
                                'edit'       => $user,
                                'type'       => 'color'
                            ])

                            @include('partials.input.yes-no', [
                                'text'       => 'Notify user by email on every new submission *',
                                'identifier' => 'notify_on_new_submissions',
                                'edit'       => $user,
                                'id1'        => 'notify_on_new_submissions_yes',
                                'id2'        => 'notify_on_new_submissions_no'
                            ])

                            @include('partials.input.yes-no', [
                                'text'       => 'Notify user by email on low entries left (applies to Free Plan only) *',
                                'identifier' => 'notify_of_low_entries',
                                'edit'       => $user,
                                'id1'        => 'notify_of_low_entries_yes',
                                'id2'        => 'notify_of_low_entries_no'
                            ])

                            @include('partials.input.submit', ['text' => 'Save'])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
