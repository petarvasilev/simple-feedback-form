@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                @include('partials.flash')

                <div class="card">
                    <div class="card-header">{{ __('Submission: ') }}#{{ $submission->id }}</div>

                    <div class="card-body">
                        <span><b>Name:</b></span>
                        <p>{{ $submission->name }}</p>

                        <span><b>Email:</b></span>
                        <p>{{ $submission->email }}</p>

                        <span><b>Message:</b></span>
                        <p>{{ $submission->message }}</p>

                        <span><b>Submitted at:</b></span>
                        <p class="zero-margin-bottom">{{ $submission->created_at_tz}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
