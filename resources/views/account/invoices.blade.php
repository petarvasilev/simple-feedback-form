@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 col-lg-8">
                @include('partials.flash')

                <div class="card">
                    <div class="card-header">{{ __('Your invoices') }}</div>
                    <div class="card-body">
                        <div class="datatable-wrapper">
                            <table class="table table-bordered datatable">
                                <thead>
                                <tr>
                                    <td><b>#</b></td>
                                    <td><b>Amount</b></td>
                                    <td><b>Currency</b></td>
                                    <td><b>Created at</b></td>
                                </tr>
                                </thead>
                                <tbody>
                                @if($invoices)
                                    @foreach($invoices as $invoice)
                                        <tr>
                                            <td>{{ $invoice->id }}</td>
                                            <td>{{ $invoice->amount }}</td>
                                            <td>{{ $invoice->amount_currency }}</td>
                                            <td>{{ $invoice->created_at }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('.datatable').DataTable({
                scrollX: '100%'
            });
        });
    </script>
@endsection
