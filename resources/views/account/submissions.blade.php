@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 col-lg-8">
                @include('partials.flash')

                <div class="card">
                    <div class="card-header">{{ __('All submissions') }}@if($dataTable){!! ' for: ' . session('free')['month'] !!}@endif</div>

                    <div class="card-body">
                        <div class="datatable-wrapper">
                            <table class="table table-bordered @if($dataTable){{ 'datatable' }}@endif @if($dataTableAjax){{ 'datatable-ajax' }}@endif">
                                <thead>
                                <tr>
                                    <td><b>#</b></td>
                                    <td><b>Name</b></td>
                                    <td><b>Email</b></td>
                                    <td><b>Message</b></td>
                                    <td><b>Submitted at</b></td>
                                </tr>
                                </thead>
                                <tbody>
                                @if($submissions)
                                    @foreach($submissions as $submission)
                                        <tr>
                                            <td><a href="{{ route('viewSubmission', ['id' => $submission->id]) }}">{{ $submission->id }}</a></td>
                                            <td>{{ $submission->name }}</td>
                                            <td>{{ $submission->email }}</td>
                                            <td>
                                                <div class="message-wrapper">
                                                    {{ $submission->message }}
                                                </div>
                                            </td>
                                            <td>{{ $submission->created_at_tz }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('.datatable').DataTable();

            $('.datatable-ajax').DataTable({
                ajax: '{{ route('submissionsAjax') }}',
                serverSide: true,
                order: [
                    [ 4, 'desc' ]
                ],
                scrollX: '100%',
                columns: [
                    {
                        data: 'id',
                        name: 'id',
                        render: function(data, type, row, meta) {
                            return "<a href='/submissions/" + data + "'>" + data + '</a>';
                        }
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'message',
                        name: 'message',
                        render: function(data, type, row, meta) {
                            return "<div class='message-wrapper'>" + data + '</div>';
                        }
                    },
                    {
                        data: 'created_at_tz',
                        name: 'created_at_tz'
                    },
                ]
            });
        });
    </script>
@endsection
