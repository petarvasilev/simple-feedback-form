@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @include('partials.flash')

                <div class="card">
                    <div class="card-header">{{ __('Update your profile') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('profilePost') }}">
                            @csrf

                            @include('partials.input.default', [
                                'text'       => 'Name *',
                                'identifier' => 'name',
                                'type'       => 'text',
                                'edit'       => $user
                                ]
                            )

                            @include('partials.input.select', [
                                'text'       => 'Country *',
                                'identifier' => 'country',
                                'edit'       => $user,
                                'values'     => $countries,
                            ])

                            @include('partials.input.default', [
                                'text' => 'Address *',
                                'identifier' => 'address',
                                'edit'       => $user,
                                'type'       => 'text',
                            ])

                            @include('partials.input.default', [
                                'text' => 'City *',
                                'identifier' => 'city',
                                'edit'       => $user,
                                'type'       => 'text',
                            ])

                            @include('partials.input.default', [
                                'text' => 'Post code *',
                                'identifier' => 'post_code',
                                'edit'       => $user,
                                'type'       => 'text',
                            ])

                            @include('partials.input.submit', ['text' => 'Save'])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
