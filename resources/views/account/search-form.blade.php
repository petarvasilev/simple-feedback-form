@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @include('partials.flash')

                <div class="card">
                    <div class="card-header">{{ __('Choose month to display submissions for') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('submissionsSearchPost') }}" autocomplete="off">
                            @csrf

                            @include('partials.input.default', [
                                'text'       => 'Month *',
                                'identifier' => 'month',
                                'type'       => 'text',
                                'placeholder'=> 'YYYY-MM'
                                ]
                            )

                            @include('partials.input.submit', ['text' => 'Search'])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('#month').monthpicker({dateFormat: 'yy-mm'});
        });
    </script>
@endsection
