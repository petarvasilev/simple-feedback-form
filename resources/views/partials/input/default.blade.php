<div class="form-group row">
    <label for="{{ $identifier }}" class="col-md-4 col-form-label text-md-right">{{ __($text) }}</label>

    <div class="col-md-6">
        <input id="{{ $identifier }}"
               type="{{ $type }}"
               class="form-control @error($identifier) is-invalid @enderror"
               name="{{ $identifier }}"
               value="@if(isset($edit)){{ $edit->$identifier }}@else{{ old($identifier) }}@endif"
               @if(isset($disabled)){{ 'disabled' }}@endif
               @if(isset($placeholder)){!! 'placeholder="' . $placeholder . '"' !!}@endif
        >

        @error($identifier)
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>