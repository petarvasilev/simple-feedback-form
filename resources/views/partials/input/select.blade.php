<div class="form-group row">
    <label for="{{ $identifier }}" class="col-md-4 col-form-label text-md-right">{{ __($text) }}</label>

    <div class="col-md-6">
        <select name="{{ $identifier }}"
                id="{{ $identifier }}"
                class="form-control @error($identifier) is-invalid @enderror"
                @if(isset($disabled)){{ 'disabled' }}@endif>
            @foreach($values as $value => $name)
                <option value="{{ $value }}" @if(isset($edit))@if($value === $edit->$identifier){!! 'selected="selected"' !!}@endif @else @if($value === old($identifier)){!! 'selected="selected"' !!}@endif @endif>{{ $name }}</option>
            @endforeach
        </select>

        @if(isset($copy))
            <div class="field-notes">
                {!! $copy !!}
            </div>
        @endif

        @error($identifier)
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>