<div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">{{ __($text) }}</label>

    <div class="col-md-6 yes-no-inputs">
        <input type="radio"
            id="{{ $id1 }}"
            name="{{ $identifier }}"
            class="@error($identifier) is-invalid @enderror"
            value="1"
            @if((isset($edit) AND $edit->$identifier == 1) OR old($identifier) == 1){{ 'checked=checked' }}@endif
            @if(isset($disabled)){{ 'disabled' }}@endif
        >
        <label for="{{ $id1 }}">{{ __('Yes') }}</label>

        <span style="width: 10px; height: 5px;">&nbsp;</span>

        <input type="radio"
               id="{{ $id2 }}"
               name="{{ $identifier }}"
               class="@error($identifier) is-invalid @enderror"
               value="0"
               @if((isset($edit) AND $edit->$identifier == 0) OR (old($identifier) === 0 OR old($identifier) === '0')){{ 'checked=checked' }}@endif
               @if(isset($disabled)){{ 'disabled' }}@endif
        >
        <label for="{{ $id2 }}">{{ __('No') }}</label>

        @error($identifier)
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>