@extends('layouts.email')

@section('content')
    <p>Hello,</p>

    <p>there is a new submission with an id of: <a href="{{ route('viewSubmission', ['id' => $submission->id]) }}">#{{ $submission->id }}</a>.</p>

    <div>
        <table width="100%" style="border: 1px solid #e7e7e7; border-collapse: collapse;">
            <tbody>
                <tr>
                    <td style="border: 1px solid #e7e7e7; vertical-align: top; padding: 4px;"><b>Name</b></td>
                    <td style="border: 1px solid #e7e7e7; vertical-align: top; padding: 4px;">{{ $submission->name }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #e7e7e7; vertical-align: top; padding: 4px;"><b>Email</b></td>
                    <td style="border: 1px solid #e7e7e7; vertical-align: top; padding: 4px;">{{ $submission->email }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #e7e7e7; vertical-align: top; padding: 4px;"><b>Message</b></td>
                    <td style="border: 1px solid #e7e7e7; vertical-align: top; padding: 4px;">{{ $submission->message }}</td>
                </tr>
            </tbody>
        </table>
    </div>

    <p>
        If you don't want receive email notifications on every new submission you can change
        your settings <a href="{{ route('settings') }}">here</a>.
    </p>
@endsection