@extends('layouts.email')

@section('content')
    <p>Hello,</p>

    <p>Your subscription to the "Epic Plan" has been deactivated.</p>
@endsection