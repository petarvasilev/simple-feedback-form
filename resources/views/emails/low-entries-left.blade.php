@extends('layouts.email')

@section('content')
    <p>Hello,</p>

    <p>
        you only have {!! config('app.notify_if_left') !!} submissions left on your current plan.
        You can upgrade through <a href="{{ url('/plans') }}">here</a>.
    </p>
@endsection