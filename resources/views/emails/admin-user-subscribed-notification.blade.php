@extends('layouts.email')

@section('content')
    <p>Hello,</p>

    <p>
        a new user subscribed to the paid plan: {{ $user->name }}, {{ $user->email }}.
    </p>
@endsection