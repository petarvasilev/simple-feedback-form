@extends('layouts.email')

@section('content')
    <p>Hello,</p>

    <p>
        Your have successfully subscribed to the "{{ config('plans.live.name') }}".
        {{ config('plans.live.amount') .
        config('plans.live.currency') }}
        a {{ strtolower(config('plans.live.frequency')) }}.
    </p>
@endsection