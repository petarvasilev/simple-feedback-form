@extends('layouts.email')

@section('content')
    <p>Hello,</p>

    <p>there is a new invoice for you with an id of: #{{ $invoice->id }}. More information below.</p>

    <div>
        <table width="100%" style="border: 1px solid #e7e7e7; border-collapse: collapse;">
            <thead>
                <tr>
                    <td style="border: 1px solid #e7e7e7; border-bottom: 2px solid #e7e7e7; padding: 4px;"><strong>{{ __('Plan') }}</strong></td>
                    <td style="border: 1px solid #e7e7e7; border-bottom: 2px solid #e7e7e7; padding: 4px; max-width: 120px;"><strong>{{ __('Description') }}</strong></td>
                    <td style="border: 1px solid #e7e7e7; border-bottom: 2px solid #e7e7e7; padding: 4px; max-width: 130px;"><strong>{{ __('Billing address') }}</strong></td>
                    <td style="border: 1px solid #e7e7e7; border-bottom: 2px solid #e7e7e7; padding: 4px;"><strong>{{ __('Amount') }}</strong></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="border: 1px solid #e7e7e7; vertical-align: top; padding: 4px;">{{ config('plans.live.name') }}</td>
                    <td style="border: 1px solid #e7e7e7; max-width: 110px; vertical-align: top; padding: 4px;">{{ config('plans.live.description') }}</td>
                    <td style="border: 1px solid #e7e7e7; max-width: 110px; vertical-align: top; padding: 4px;">{{ $user->getBillingAddress() }}</td>
                    <td style="border: 1px solid #e7e7e7; vertical-align: top; padding: 4px;">{{ $invoice->amount }}{{ $invoice->amount_currency }}</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3" style="text-align: right; border: 1px solid #e7e7e7; padding: 4px;">{{ __('Total paid:') }}</td>
                    <td style="border: 1px solid #e7e7e7; padding: 4px;">{{ $invoice->amount }}{{ $invoice->amount_currency }}</td>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection