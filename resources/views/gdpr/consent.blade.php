<div id="cookies-consent-popup" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">GDPR compliance</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="cookies-alert" class="alert alert-warning">
                    To login, register or reset password you need to at least agree
                    to the functional cookies. Just click save preferences and you'll be good to go.
                </div>

                <p>
                    In order for this website to function properly we use cookies. We, also,
                    store your ip address for tracking purposes. To learn more go to our
                    <a href="/privacy-policy">Privacy Policy.</a>
                </p>

                <p>
                    We use three main types of cookies, here they are:
                </p>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <td>Type</td>
                            <td>Description</td>
                            <td>Agree</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach(config('cookies.types') as $id => $cookieDetails)
                            <tr class="cookie-type" data-id="{{ $id }}">
                                <td>
                                    {{ $cookieDetails['name'] }}
                                </td>
                                <td>{{ $cookieDetails['description'] }}</td>
                                <td>
                                    <input type="checkbox"
                                           name="{{ $id }}_agree"
                                           id="{{ $id }}_agree"
                                    @if($cookieDetails['required'] OR \App\Logic\GDPR\Consent::isGiven($id)){!! 'checked=checked' !!}@endif
                                    @if($cookieDetails['required']){!! 'disabled=disabled' !!}@endif
                                    >
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save-preferences">Save preferences</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        var data = {};

        $('#save-preferences').on('click', function() {
            $('.cookie-type').each(function(index, item) {
                var that = $(item);
                var id = that.data('id');

                if ($('input[name=' + id + '_agree]').is(':checked')) {
                    data[id] = 1;
                } else {
                    data[id] = 0;
                }
            });

            $.post('/gdpr/cookies/consent', data, function(data) {
                if (data.status === 'OK') {
                    alert('Your cookie preferences were successfully saved.');
                    $('#cookies-consent-popup').modal('hide');
                    $('body').removeClass('functional-cookies-consent-is-not-given')
                            .addClass('functional-cookies-consent-is-given');

                    window.location.replace(window.location.href);
                }
            });
        });
    });
</script>