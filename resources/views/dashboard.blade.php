@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @include('partials.flash')

            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>
                <div class="card-body">
                    <p>
                        @if($user->isSubscribed())
                            You are subscribed to the "{{ config('plans.' . config('services.paypal.mode') . '.name') }}". {{ config('plans.' . config('services.paypal.mode') . '.description') }}
                            <form class="cancel-subscription-form" action="{{ route('cancelSubscription') }}" method="post" onsubmit="if(!confirm('Are you sure you want to cancel your subscription?')){event.preventDefault();}">
                                @csrf

                                <input class="btn btn-danger" type="submit" value="Cancel subscription" id="cancel-button">
                            </form>
                        @elseif($user->isOnGracePeriod())
                            You are on your grace period for the "Epic Plan" until {{ $user->grace_period_until }} GMT+{{ \App\Logic\Helper::gmtOffset() }}.
                        @else
                            Your are on the "Free Plan". {{ config('plans.free.limit') }} entries/month.
                        @endif
                    </p>

                    <p>
                        Your API key is: {{ $user->api_key }}
                    </p>

                    <div id="website-code">
                        <p>To include the form on your website include the following code in your
                            website's &lt;head&gt; tag:</p>

                        <pre><code class="language-javascript">{{ $code }}</code></pre>
                    </div>

                    <p>
                        To view all the feedback/submissions from your form go to <a
                                href="{{ route('submissions') }}">{{ __('Submissions') }}</a>.
                    </p>

                    <p>
                        To change your form settings go to <a href="{{ route('settings') }}">{{ __('Settings') }}</a>.
                    </p>

                    <p>
                        To change your profile information including billing address go to <a
                                href="{{ route('profile') }}">{{ __('Profile') }}</a>.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
