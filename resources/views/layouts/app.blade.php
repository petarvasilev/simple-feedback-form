<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if(isset($pageTitle)){{ $pageTitle . ' | ' }}@endif{{ config('app.name', 'Simple Feedback') }}</title>

    <link rel="icon"
          type="image/png"
          href="{{ asset('/img/favicon.png') }}">

    @if(isset($metaDescription))
        <meta name="description" content="{{ $metaDescription }}">
    @endif

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @if(\App\Logic\GDPR\Consent::isGiven('analytical'))
        @include('partials.google-analytics')
    @endif

    @if(\App\Logic\GDPR\Consent::isGiven('recaptcha'))
        {!! NoCaptcha::renderJs() !!}
    @endif

    @if(config('app.simple-feedback-form-key'))
        <link rel='stylesheet' href='https://www.simplefeedbackform.com/css/include.v1.css'>
        <script src="https://www.simplefeedbackform.com/js/include.v1.js"></script>
        <script>
            var simpleFeedbackForm = new SimpleFeedbackForm('{{ config('app.simple-feedback-form-key') }}')
        </script>
    @endif
</head>
<body class="@if(\App\Logic\GDPR\Consent::isGiven('functional')){{ 'functional-cookies-consent-is-given' }} @else {{ 'functional-cookies-consent-is-not-given' }} @endif">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img id="logo" src="{{ config('assets.logo') }}"
                         title="{{ config('app.name', 'Simple Feedback Form') }}"
                         alt="{{ config('app.name', 'Simple Feedback Form') }}">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('homepage') }}">{{ __('Home') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('plans') }}">{{ __('Plans') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('submissionsSearch') }}">{{ __('Submissions') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('settings') }}">{{ __('Settings') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('plans') }}">{{ __('Plans') }}</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('profile') }}">
                                        {{ __('Profile') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('invoices') }}">
                                        {{ __('Invoices') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('exportData') }}">
                                        {{ __('Export data') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <footer>
        <div class="content">
            <p>
                <a id="cookie-settings-link" href="#">cookie settings</a>
                <span>:</span>
                <a href="{{ route('privacyPolicy') }}">privacy policy</a>
            </p>

            <p>
                This website is open-source, the source code can be found <a href="https://bitbucket.org/petarvasilev/simple-feedback-form/">here</a>.
            </p>
            {{ date('Y') }} &copy; {{ config('app.name') }}
        </div>
    </footer>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <script src="{{ asset('/js/libraries/datatables.jquery.min.js') }}"></script>
    <script src="{{ asset('/js/libraries/datatables.bootstrap4.min.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('/css/datatables.css') }}">

    @yield('scripts')

    @include('gdpr.consent')
</body>
</html>
