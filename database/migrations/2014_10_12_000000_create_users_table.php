<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->tinyInteger('role')->default(1);
            $table->string('api_key', 100)->unique();
            $table->tinyInteger('subscription_status')->default(0);
            $table->string('agreement_id', 200)->nullable()->default(null);
            $table->string('payer_id', 200)->nullable()->default(null);
            $table->dateTime('paypal_subscription_starts_at')->nullable()->default(null);
            $table->dateTime('grace_period_until')->nullable()->default(null);
            $table->dateTime('subscription_activated_at')->nullable()->default(null);
            $table->tinyInteger('notify_of_low_entries')->default(1);
            $table->tinyInteger('notify_on_new_submissions')->default(1);
            $table->string('ip_address', 50);
            $table->string('country', 300)->default('');
            $table->string('address', 400)->default('');
            $table->string('city', 200)->default('');
            $table->string('post_code', 100)->default('');
            $table->string('position', 250)->default('bottom right');
            $table->string('background_color', 100)->default('#2196F3');
            $table->string('form_background_color', 100)->default('#2196F3');
            $table->rememberToken();
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
