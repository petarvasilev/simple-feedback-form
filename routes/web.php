<?php

Route::get('/', 'PagesController@homepage')->name('homepage');

Auth::routes();

Route::get('/dashboard', 'DashboardController@dashboard')->name('dashboard');

Route::get('/plans', 'PagesController@plans')->name('plans');
Route::get('/paypal/cancelled', 'PagesController@cancelled')
    ->middleware('auth')
    ->name('cancelled');
Route::get('/privacy-policy', 'PagesController@privacyPolicy')->name('privacyPolicy');
Route::post('/gdpr/cookies/consent', 'GDPRController@cookiesConsent');

# Account stuff
Route::get('/submissions', 'AccountController@submissions')
    ->name('submissions');
Route::get('/submissions/search', 'AccountController@searchForm')
    ->name('submissionsSearch');
Route::post('/submissions/search', 'AccountController@searchPost')
    ->name('submissionsSearchPost');
Route::get('/submissions/{id}', 'AccountController@submissionShow')
    ->name('viewSubmission');
Route::get('/submissions/ajax/json', 'AccountController@submissionsAjax')
    ->name('submissionsAjax');

Route::get('/settings', 'AccountController@settings')
    ->name('settings');
Route::post('/settings', 'AccountController@settingsPost')
    ->name('settingsPost');

Route::get('/profile', 'AccountController@profile')
    ->name('profile');
Route::post('/profile', 'AccountController@profilePost')
    ->name('profilePost');

Route::post('/subscription/cancel', 'AccountController@cancelSubscription')
    ->name('cancelSubscription');

Route::get('/invoices', 'AccountController@invoices')
    ->name('invoices');

Route::get('/export-data', 'AccountController@exportData')
    ->name('exportData');

# Storing submissions
Route::post('/submissions', 'SubmissionsController@store')
    ->middleware('cors');

# Getting settings
Route::post('/settings/get', 'SettingsController@get')
    ->middleware('cors');

# PayPal
## admin routes
Route::get('/paypal/admin/bootstrap', 'PayPalController@bootstrap')
    ->middleware('auth', 'is_admin');
Route::get('/paypal/admin/activate-subscription', 'PayPalController@activateSubscription')
    ->middleware('auth', 'is_admin');
Route::get('/paypal/plans/show', 'PayPalController@showPlans')
    ->middleware('auth', 'is_admin');

Route::get('/admin/test-rollbar', function() {
    \Log::debug('Testing rollbar.');

    return 'Success';
})->middleware('auth', 'is_admin');

## user routes
Route::get('/paypal/plans/epic/subscribe', 'PayPalController@subscribeEpic') // redirect to PayPal for subscription approval
    ->middleware('auth')
    ->name('subscribeEpic');

## paypal routes
Route::get('/paypal/agreement', 'PayPalController@agreement')->name('agreement');
Route::post('/paypal/webhook', 'PayPalController@webhook');
Route::get('/paypal/available-webhooks', 'PayPalController@availableWebhookEvents');