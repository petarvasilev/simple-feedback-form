<?php

return [
    /**
     * Information about the plan, note there is only one plan,
     * though there are two configurations live and sandbox
     */
    'sandbox' => [
        'name' => 'Test Epic Plan',
        'description' => '€10 a day for unlimited form submissions.',
        'subscription-name' => 'Subscribing to Test Epic Plan',
        'subscription-description' => '€10 a day for unlimited form submissions.',
        'amount' => 10, // 10 euro
        'currency' => 'EUR',
        'frequency' => 'Day',
        'frequency-interval' => 1,
        'setup-fee' => 10,
        'limit' => 'unlimited',
    ],
    'live' => [
        'name' => 'Epic Plan',
        'description' => '€10 a month for unlimited form submissions.',
        'subscription-name' => 'Subscribing to Epic Plan',
        'subscription-description' => '€10 a month for unlimited form submissions.',
        'amount' => 10, // 10 euro
        'currency' => 'EUR',
        'frequency' => 'Month',
        'frequency-interval' => 1,
        'setup-fee' => 10,
        'limit' => 'unlimited',
    ],
    'free' => [
        'name' => 'Free Plan',
        'description' => config('plans.free.limit') . ' entries/month for €0/month.',
        'limit' => 150
    ]
];