<?php

return [
    'settings-cookie-name' => 'settings',

    'default-cookie-expiration' => 60*24*365, // in minutes

    /**
     * The types of cookies you are collecting consent for
     */
    'types' => [
        /**
         * functional cookies details must be present for the consent solution to work
         */
        'functional' => [
            'name' => 'Strictly necessary',
            'description' => 'Cookies which are used to save your preferences as well as log you in our website.',
            'required' => true,
        ],
        'recaptcha' => [
            'name' => 'Recaptcha',
            'description' => 'Required in order to register on our website. Used for validating that you are human. Tracks personal information.',
            'required' => true
        ],
        'analytical' => [
            'name' => 'Analytical',
            'description' => 'Cookies used for tracking users and measuring traffic. In our case Google Analytics.',
            'required' => false,
            'to-remove' => [
                '_ga',
                '_gid',
                '_gat_gtag_' . str_replace('-', '_', config('analytics.google-analytics-id'))
            ], // cookies to remove if consent is withdrawn
        ]
    ]
];