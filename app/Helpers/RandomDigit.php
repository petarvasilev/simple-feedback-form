<?php

namespace App\Helpers;

class RandomDigit {

    static function gen($length = 12)
    {
        $result = '';

        for ($i = 0; $i < $length; $i++) {
            $random = rand(0, 9);
            $result .= $random;
        }

        return $result;
    }

}