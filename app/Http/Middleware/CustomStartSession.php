<?php

namespace App\Http\Middleware;

use Closure;
use App\Logic\GDPR\Consent;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\Support\Facades\Cookie;

class CustomStartSession extends StartSession {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $this->sessionConfigured()) {
            return $next($request);
        }

        $currentConsent = [];
        $consentedFunctional = Consent::isGiven('functional');
        $cookieTypes = config('cookies.types');

        foreach ($cookieTypes as $type => $details) {
            $currentConsent[$type] = (int)$request->get($type);
        }

        if ($consentedFunctional OR $currentConsent['functional'] === 1) {
            // If a session driver has been configured, we will need to start the session here
            // so that the data is ready for an application. Note that the Laravel sessions
            // do not make use of PHP "native" sessions in any way since they are crappy.
            $request->setLaravelSession(
                $session = $this->startSession($request)
            );

            $this->collectGarbage($session);

            if ($currentConsent['functional'] === 1) {
                Consent::set($currentConsent);
            }

            // if consent withdrawn remove specified cookies
            foreach ($cookieTypes as $type => $details) {
                if (isset($currentConsent[$type]) AND $currentConsent[$type] == 0) {
                    if ( ! $details['required']) {
                        foreach ($details['to-remove'] as $cookieName) {
                            Cookie::queue(Cookie::forget($cookieName, '/', config('app.domain')));
                        }
                    }
                }
            }

            $response = $next($request);

            $this->storeCurrentUrl($request, $session);

            $this->addCookieToResponse($response, $session);

            // Again, if the session has been configured we will need to close out the session
            // so that the attributes may be persisted to some storage medium. We will also
            // add the session identifier cookie to the application response headers now.
            $this->saveSession($request);

            return $response;
        } else {
            return $next($request);
        }
    }
}