<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if not logged in return false
        if ( ! Auth::check()) {
            abort(403);
        }

        $user = Auth::user();

        // if not admin return false
        if ($user->role !== 10) {
            abort(403);
        }

        return $next($request);
    }
}
