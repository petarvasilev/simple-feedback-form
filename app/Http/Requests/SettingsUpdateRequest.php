<?php

namespace App\Http\Requests;

use App\Rules\ValidColor;
use App\Rules\ValidPosition;
use Illuminate\Foundation\Http\FormRequest;

class SettingsUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'position' => ['required', new ValidPosition()],
            'background_color' => ['required', new ValidColor()],
            'form_background_color' => ['required', new ValidColor()],
            'notify_on_new_submissions' => 'required|in:1,0',
            'notify_of_low_entries' => 'required|in:1,0',
        ];
    }
}
