<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function get(Request $request)
    {
        $apiKey = $request->get('key');

        $user = User::where('api_key', $apiKey)->first();

        if ( ! $user) {
            return response()->json(['status' => 'BAD', 'message' => 'Invalid API key.']);
        }

        return response()->json([
            'status' => 'OK',
            'position' => $user->position,
            'background_color' => $user->background_color,
            'form_background_color' => $user->form_background_color,
        ]);
    }
}
