<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileUpdateRequest;
use App\Http\Requests\SettingsUpdateRequest;
use App\Http\Requests\SubmissionsSearchRequest;
use App\Logic\Helper;
use App\Logic\PayPal\PayPal;
use App\Submission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function submissionShow($id)
    {
        $user = Auth::user();

        $submission = Submission::where('user_id', $user->id)
            ->where('id', $id)
            ->first();

        if ( ! $submission) {
            Session::flash('error', 'Submission not found.');

            return redirect(route('dashboard'));
        }

        return view('account.submission-show')
            ->with('submission', $submission)
            ->with('pageTitle', 'Showing submission');
    }

    public function submissionsAjax()
    {
        $user = Auth::user();

        if ($user->isSubscribed() OR $user->isOnGracePeriod()) {
            $model = Submission::select('submissions.*')
                ->where('user_id', $user->id);

            return DataTables::of($model)->make();
        }
    }

    public function submissions()
    {
        $user = Auth::user();
        $dataTable = false;
        $dataTableAjax = false;

        if ($user->isSubscribed() OR $user->isOnGracePeriod()) {
            $dataTableAjax = true;
            $submissions = false;
        } else {
            $free = session('free');
            $dataTable = true;

            $submissions = Submission::where('user_id', $user->id)
                ->where('created_at', '>=', $free['start'])
                ->where('created_at', '<=', $free['end'])
                ->orderBy('created_at', 'ASC')
                ->limit(config('plans.free.limit'))
                ->get();
        }

        return view('account.submissions', [
            'submissions' => $submissions,
            'dataTable' => $dataTable,
            'dataTableAjax' => $dataTableAjax,
            'pageTitle' => 'Submissions'
        ]);
    }

    public function searchForm()
    {
        $user = Auth::user();

        if ($user->isSubscribed() OR $user->isOnGracePeriod()) {
            return redirect(route('submissions'));
        }

        return view('account.search-form', ['pageTitle' => 'Search Submissions']);
    }

    public function searchPost(Request $request, SubmissionsSearchRequest $submissionsSearchRequest)
    {
        $yearMonth = $request->get('month');

        Helper::setSubmissionsRangeForFreePlanUser($yearMonth);

        return redirect(route('submissions'));
    }

    public function settings()
    {
        $positionChoices = Helper::formPlacementChoicesForSelect();
        $user = Auth::user();

        return view('account.settings', [
            'positionChoices' => $positionChoices,
            'user' => $user,
            'pageTitle' => 'Settings',
        ]);
    }

    public function settingsPost(Request $request, SettingsUpdateRequest $settingsUpdateRequest)
    {
        $input = Helper::unsetSome($request->all());

        $user = Auth::user();
        $user->fill($input);
        $user->save();

        Session::flash('success', __('Settings successfully updated.'));

        return back();
    }

    public function profile()
    {
        $user = Auth::user();
        $countries = Helper::countriesForSelect();

        return view('account.profile', [
            'user' => $user,
            'countries' => $countries,
            'pageTitle' => 'Update your profile'
        ]);
    }

    public function profilePost(Request $request, ProfileUpdateRequest $profileUpdateRequest)
    {
        $input = Helper::unsetSome($request->all());

        $user = Auth::user();

        $user->fill($input);
        $user->save();

        Session::flash('success', __('Profile successfully updated.'));

        return back();
    }

    public function cancelSubscription()
    {
        $user = Auth::user();

        if ( ! $user->isSubscribed()) {
            Session::flash('error', "You can't cancel because you are not subscribed.");

            return back();
        }

        if ($user->subscribedWithin15Minutes()) {
            Session::flash('error', 'You can only cancel your subscription if 15 minutes have passed since subscribing.');

            return back();
        }

        $paypal = new PayPal();
        $paypal->suspendAgreement($user->agreement_id);

        $user->deactivateSubscription();

        Session::flash('success', "Your subscription was successfully cancelled.");

        return back();
    }

    public function invoices()
    {
        $user = Auth::user();
        $invoices = $user->invoices;

        return view('account.invoices', [
            'invoices' => $invoices,
            'pageTitle' => 'Your invoices',
        ]);
    }

    public function exportData()
    {
        $user = Auth::user();

        $profileData = [
            'name' => $user->name,
            'email' => $user->email,
            'address' => $user->getBillingAddress(false)
        ];

        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];

        return response()->json($profileData, 200, $headers, JSON_UNESCAPED_UNICODE);
    }
}
