<?php

namespace App\Http\Controllers;

use App\Logic\PayPal\PayPal;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PayPalController extends Controller
{
    public function bootstrap()
    {
        $setting = Setting::where('name', '=', 'active_plan_id')->first();

        if ($setting) {
            return response()->json(['message' => 'Plan already created & activated.']);
        }

        $paypal = new PayPal();
        $response = $paypal->bootstrap();

        return response()->json(['message' => $response]);
    }

    public function showPlans()
    {
        $paypal = new PayPal();

        return $paypal->showPlans();
    }

    public function subscribeEpic()
    {
        $user = Auth::user();

        if ($user->isSubscribed()) {
            Session::flash('error', 'You are already subscribed.');

            return back();
        }

        $paypal = new PayPal();

        return $paypal->createSubscription();
    }

    public function agreement(Request $request)
    {
        $paypal = new PayPal();

        return $paypal->agreement($request);
    }

    public function availableWebhookEvents()
    {
        $paypal = new PayPal();

        return $paypal->availableWebhookEvents();
    }

    public function webhook()
    {
        $paypal = new PayPal();

        $paypal->webhook();
    }

    public function activateSubscription()
    {
        $user = Auth::user();
        $user->activateSubscription();
    }
}
