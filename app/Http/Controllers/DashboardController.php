<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        $user = Auth::user();

        $code = "<link rel='stylesheet' href='https://www.simplefeedbackform.com/css/include.v1.css'>
<script src=\"https://www.simplefeedbackform.com/js/include.v1.js\"></script>
<script>
    var simpleFeedbackForm = new SimpleFeedbackForm('{$user->api_key}')
</script>";

        return view('dashboard', [
            'user' => $user,
            'code' => $code,
            'pageTitle' => 'Dashboard'
        ]);
    }
}
