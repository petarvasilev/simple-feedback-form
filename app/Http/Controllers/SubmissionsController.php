<?php

namespace App\Http\Controllers;

use App\Logic\Helper;
use App\Submission;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PHPUnit\TextUI\Help;

class SubmissionsController extends Controller
{
    public function store(Request $request)
    {
        $input = $request->all();

        $apiKey = $input['key'];

        $validator = Validator::make($request->all(), [
            'key' => 'required|exists:users,api_key',
            'name' => 'required|string|max:200',
            'email' => 'required|email|max:200',
            'message' => 'required|string|max:16000',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'BAD', 'errors' => $validator->errors()]);
        }

        $user = User::where('api_key', $apiKey)->first();
        $input['user_id'] = $user->id;
        $input['ip_address'] = $_SERVER['REMOTE_ADDR'];

        // save created at with timezone information
        $createdAt = Carbon::now()->format('Y-m-d H:i:s');

        if (Helper::summerTime($createdAt)) {
            $createdAt .= ' EEST';
        } else {
            $createdAt .= ' EET';
        }

        $input['created_at_tz'] = $createdAt;

        $submission = new Submission();
        $submission->fill($input);
        $submission->save();

        Helper::notifyUserOfLowEntriesLeft($user);
        Helper::notifyUserOfNewSubmission($user, $submission);

        return response()->json(['status' => 'OK', 'message' => 'Submission successfully saved.']);
    }
}
