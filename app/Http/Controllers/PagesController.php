<?php

namespace App\Http\Controllers;

class PagesController extends Controller
{
    public function plans()
    {
        return view('pages.plans')
            ->with('pageTitle', __('Plans'))
            ->with('metaDescription', __('Choose from a free and paid plans.'));
    }

    public function homepage()
    {
        return view('pages.homepage')
            ->with('pageTitle', __('Home'))
            ->with('metaDescription', __('We provide feedback form for your website or said otherwise we are feedback form as a service. Integrates easily into your website.'));
    }

    public function cancelled()
    {
        return view('pages.cancelled')
            ->with('pageTitle', __('Cancelled'));
    }

    public function privacyPolicy()
    {
        return view('pages.privacy-policy')
            ->with('pageTitle', __('Privacy Policy'));
    }
}
