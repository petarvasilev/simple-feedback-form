<?php

namespace App\Logic\PayPal;

use PayPal\Auth\OAuthTokenCredential;

trait ApiContextTrait {

    private $apiContext;

    public function setCredentials()
    {
        $this->apiContext = new \PayPal\Rest\ApiContext(
            new OAuthTokenCredential(
                config('services.paypal.client-id'),// Client ID
                config('services.paypal.secret')    // Client secret
            )
        );

        // if mode is live go live
        if (config('services.paypal.mode') === 'live') {
            $this->apiContext->setConfig(
                [
                    'mode' => 'live',
                ]
            );
        }
    }

}
