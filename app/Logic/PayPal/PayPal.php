<?php

namespace App\Logic\PayPal;

use App\Invoice;
use App\Jobs\SendInvoiceMailJob;
use App\Logic\Helper;
use App\Setting;
use App\User;
use App\Webhook;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use PayPal\Api\Agreement;
use PayPal\Api\AgreementStateDescriptor;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\Payer;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Plan;
use PayPal\Api\VerifyWebhookSignature;
use PayPal\Api\WebhookEvent;
use PayPal\Api\WebhookEventType;
use PHPUnit\TextUI\Help;

class PayPal
{
    use ApiContextTrait;

    public function __construct()
    {
        $this->setCredentials();
    }

    /**
     * Performs one time PayPal operations such as
     * creating and activating a plan
     *
     * @return mixed
     */
    public function bootstrap()
    {
        // create plan
        $planDetails = $this->createPlan();

        // activate plan
        $this->activatePlan($planDetails);

        // save plan id in database
        $setting = new Setting(
            [
                'name' => 'active_plan_id',
                'value' => $planDetails->id,
            ]
        );
        $setting->save();

        return 'Plan created and activated.';
    }

    /**
     * Create a billing plan
     *
     * @return Plan
     */
    public function createPlan()
    {
        $mode = config('services.paypal.mode');

        $planDetails = config('plans')[$mode];

        $plan = new Plan();
        $plan->setName($planDetails['name'])
            ->setDescription($planDetails['description'])
            ->setType('INFINITE'); // or FIXED: The plan has a fixed number of payment cycles

        $paymentDefinition = new PaymentDefinition();
        $paymentDefinition->setName('Regular Payments')
            ->setType('REGULAR') // or TRIAL
            ->setFrequency($planDetails['frequency']) // or WEEK, DAY, YEAR, MONTH
            ->setFrequencyInterval($planDetails['frequency-interval']) // The interval at which the customer is charged. Value cannot be greater than 12 months
            ->setAmount(new Currency(['value' => $planDetails['amount'], 'currency' => $planDetails['currency']]));
        // ->setCycles("12")

        $merchantPreferences = new MerchantPreferences();
        $merchantPreferences->setReturnUrl(route('agreement',['success'=>'true'])) // The URL where the customer can approve the agreement
            ->setCancelUrl(route('agreement',['success'=>'false'])) // The URL where the customer can cancel the agreement
            ->setAutoBillAmount("yes") // Allowed values: YES, NO. Default is NO. Indicates whether PayPal automatically bills the outstanding balance in the next billing cycle
            ->setInitialFailAmountAction("CANCEL") // Action to take if a failure occurs during initial payment. Allowed values: CONTINUE, CANCEL. Default is continue
            ->setMaxFailAttempts("1") // Total number of failed attempts allowed. Default is 0, representing an infinite number of failed attempts.
            ->setSetupFee(
                new Currency(
                    ['value' => $planDetails['setup-fee'], 'currency' => $planDetails['currency']]
                )
        ); // The currency and amount of the set-up fee for the agreement. This fee is the initial, non-recurring payment amount that is due immediately when the billing agreement is created.


        $plan->setPaymentDefinitions(array($paymentDefinition));
        $plan->setMerchantPreferences($merchantPreferences);

        $createdPlan = $plan->create($this->apiContext);

        return $createdPlan;
    }

    /**
     * Activate a billing plan
     *
     * @param $plan
     */
    public function activatePlan(Plan $plan)
    {
        $patch = new \PayPal\Api\Patch();
        $value = new \PayPal\Common\PayPalModel('{
           "state":"ACTIVE"
        }');
        $patch->setOp('replace')
            ->setPath('/')
            ->setValue($value);

        $patchRequest = new \PayPal\Api\PatchRequest();
        $patchRequest->addPatch($patch);

        $plan->update($patchRequest, $this->apiContext);
    }

    /**
     * Shows all plans, 5 per page
     *
     * @return array
     */
    public function showPlans()
    {
        $params = [
            'page_size' => '5',
            'status'    =>'ALL' // CREATED, ACTIVE, INACTIVE, ALL
        ];

        $planList = Plan::all($params, $this->apiContext);

        return $planList->toArray();
    }


    public function createSubscription()
    {
        $mode = config('services.paypal.mode');

        $planDetails = config('plans')[$mode];

        $activePlanID = Helper::getActivePlanID();

        if ($mode === 'sandbox') {
            $date = Helper::oneDayIntoTheFuture();
            $dbDate = Helper::oneDayIntoTheFuture(true);
        } else {
            $date = Helper::oneMonthIntoTheFuture();
            $dbDate = Helper::oneMonthIntoTheFuture(true);
        }

        $user = Auth::user();
        $user->paypal_subscription_starts_at = $dbDate;
        $user->save();

        $agreement = new Agreement();
        $agreement->setName($planDetails['subscription-name'])
            ->setDescription($planDetails['subscription-description']) // we can start date to 1 month from now if we take our first payment via the setup fee
            ->setStartDate($date);

        $plan = new Plan();
        $plan->setId($activePlanID);
        $agreement->setPlan($plan);

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $agreement->setPayer($payer);

        $agreement = $agreement->create($this->apiContext);
        $approvalUrl = $agreement->getApprovalLink();

        return redirect($approvalUrl);
    }

    /**
     * Execute agreement and if successful activate
     * subscription
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function agreement(Request $request)
    {
        $success = $request->get('success');

        if (!empty($success))
        {
            $token = $request->get('token');

            if ($success === 'true' && !empty($token))
            {
                $agreement = new Agreement();

                $agreement->execute($token, $this->apiContext);

                $agreement = $agreement->toArray();

                // save agreement in database for debugging
                $webhook = new Webhook();
                $webhook->event = 'Agreement started. Saving agreement data.';
                $webhook->json = json_encode($agreement);
                $webhook->save();

                $user = Auth::user();
                $user->agreement_id = $agreement['id'];
                $user->payer_id = $agreement['payer']['payer_info']['payer_id'];
                $user->save();

                $user->activateSubscription();

                Session::flash('success', 'Your subscription was successfully activated.');

                return redirect()->route('dashboard');
            }
            else
            {
                return redirect(route('cancelled'));
            }
        }
    }

    /**
     * Fetch a PayPal agreement
     *
     * @param $agreementID
     * @return Agreement
     */
    public function getAgreement($agreementID)
    {
        $agreement = Agreement::get($agreementID, $this->apiContext);

        return $agreement;
    }

    /**
     * Suspend an agreement
     *
     * @param $agreementID
     * @return bool
     */
    public function suspendAgreement($agreementID)
    {
        // Create an Agreement State Descriptor, explaining the reason to suspend.
        $agreementStateDescriptor = new AgreementStateDescriptor();
        $agreementStateDescriptor->setNote("Suspending the agreement");

        // get agreement
        $agreement = $this->getAgreement($agreementID);

        // suspend agreement
        try {
            $agreement->suspend($agreementStateDescriptor, $this->apiContext);
        } catch (Exception $e) {
            logger($e->getData());
            exit(1);
        }

        return true;
    }

    public function availableWebhookEvents()
    {
        return WebhookEventType::availableEventTypes($this->apiContext);
    }

    public function extractAgreementID($responseArray)
    {
        $links = $responseArray['webhook_event']['resource']['links'];
        $href = false;

        foreach ($links as $link) {
            if ($link['rel'] === 'cancel') {
                $href = $link['href'];
            }
        }

        if ( ! $href) {
            throw new Exception('Cancel link not found.');
        }

        $split = explode('/billing-agreements/', $href);
        $split = explode('/cancel', $split);
        $agreementID = $split[1];

        return $agreementID;
    }

    /**
     * Handle certain PayPal events
     */
    public function webhook()
    {
        /**
         * Receive the entire body that you received from PayPal webhook.
         */
        $bodyReceived = file_get_contents('php://input');

        // Receive HTTP headers that you received from PayPal webhook.
        $headers = getallheaders();

        /**
         * Uppercase all the headers for consistency
         */
        $headers = array_change_key_case($headers, CASE_UPPER);

        $signatureVerification = new VerifyWebhookSignature();
        $signatureVerification->setWebhookId(config('services.paypal.webhook-id'));
        $signatureVerification->setAuthAlgo($headers['PAYPAL-AUTH-ALGO']);
        $signatureVerification->setTransmissionId($headers['PAYPAL-TRANSMISSION-ID']);
        $signatureVerification->setCertUrl($headers['PAYPAL-CERT-URL']);
        $signatureVerification->setTransmissionSig($headers['PAYPAL-TRANSMISSION-SIG']);
        $signatureVerification->setTransmissionTime($headers['PAYPAL-TRANSMISSION-TIME']);

        $webhookEvent = new WebhookEvent();
        $webhookEvent->fromJson($bodyReceived);
        $signatureVerification->setWebhookEvent($webhookEvent);
        $request = clone $signatureVerification;

        try {
            $output = $signatureVerification->post($this->apiContext);
        } catch (Exception $e) {
            info($e->getMessage());
            exit(1);
        }

        $verificationStatus = $output->getVerificationStatus();

        $responseJSON = $request->toJSON();
        $responseArray = json_decode($responseJSON, true);

        $event = $responseArray['webhook_event']['event_type'];

        if ($verificationStatus == 'SUCCESS')
        {
            // save webhook in database
            $webhook = new Webhook(
                [
                    'event' => $event,
                    'json' => $responseJSON,
                ]
            );
            $webhook->save();

            switch($event)
            {
                case 'BILLING.SUBSCRIPTION.CANCELLED':
                case 'BILLING.SUBSCRIPTION.SUSPENDED':
                case 'BILLING.SUBSCRIPTION.EXPIRED':
                case 'BILLING_AGREEMENTS.AGREEMENT.CANCELLED':
                    $payerID = $responseArray['webhook_event']['resource']['payer']['payer_info']['payer_id'];
                    $agreementID = $this->extractAgreementID($responseArray);
                    logger('Cancelling agreement. Agreement id: ' .$agreementID);

                    $user = User::where('payer_id', $payerID)
                        ->where('agreement_id', $agreementID)
                        ->first();
                    $user->deactivateSubscription();

                    break;

                case 'PAYMENT.SALE.COMPLETED':
                    $agreementID = $responseArray['webhook_event']['resource']['billing_agreement_id'];

                    $user = User::where('agreement_id', $agreementID)->first();

                    if (! $user) {
                        throw new Exception('User not found by agreement id: ' . $agreementID);
                    }

                    $user->extendGracePeriod();

                    $invoiceData = [
                        'user_id' => $user->id,
                        'agreement_id' => $agreementID,
                        'amount' => $responseArray['webhook_event']['resource']['amount']['total'],
                        'amount_currency' => $responseArray['webhook_event']['resource']['amount']['currency'],
                        'paypal_fee_amount' => $responseArray['webhook_event']['resource']['transaction_fee']['value'],
                        'paypal_fee_currency' => $responseArray['webhook_event']['resource']['transaction_fee']['value'],
                    ];

                    $invoice = new Invoice($invoiceData);
                    $invoice->save();

                    SendInvoiceMailJob::dispatch($user, $invoice);

                    break;
            }
        }

        echo $verificationStatus;

        exit(0);
    }
}