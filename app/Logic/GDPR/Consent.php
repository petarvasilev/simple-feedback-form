<?php

namespace App\Logic\GDPR;

use Exception;
use Illuminate\Support\Facades\Cookie;

class Consent {

    static function isGiven($cookieType)
    {
        $settings = Cookie::get(config('cookies.settings-cookie-name'));

        try {
            $settings = json_decode($settings, true);
        } catch (Exception $e) {
            logger('Invalid JSON in the settings cookie.');
            exit(1);
        }

        if (isset($settings['allowedCookies'][$cookieType])) {
            if ($settings['allowedCookies'][$cookieType] === 1) {
                return true;
            }
        }

        return false;
    }

    static function set($cookies)
    {
        $cookieValue = json_encode(['allowedCookies' => $cookies]);

        Cookie::queue(config('cookies.settings-cookie-name'), $cookieValue, config('cookies.default-cookie-expiration'));
    }
}