<?php

namespace App\Logic;

use App\Jobs\SendNewSubmissionMailJob;
use App\Jobs\SendNotifyUserOfLowEntriesLeftMailJob;
use App\Setting;
use App\Submission;
use App\User;
use DateTime;
use Exception;
use Illuminate\Support\Facades\DB;

class Helper
{
    static function getActivePlanID()
    {
        $setting = Setting::where('name', 'active_plan_id')
            ->orderBy('id', 'DESC')
            ->limit(1)
            ->first();

        if ($setting) {
            return $setting->value;
        }

        throw new Exception('Setting "active_plan_id" not found.');
    }

    /**
     * Returns a date 1 month in the future formatted
     * either as PayPal requires or as MySQL requires
     *
     * @param bool $mysql if true formats the date as MySQL requires if false as PayPal
     * @return false|string
     */
    static function oneMonthIntoTheFuture($mysql = false)
    {
        $format = self::datetimeFormat($mysql);

        return gmdate($format, strtotime("+1 month", time()));
    }

    /**
     * Returns a datetime one day into the future
     *
     * @param bool $mysql if true formats the date as MySQL requires if false as PayPal
     * @return false|string
     */
    static function oneDayIntoTheFuture($mysql = false)
    {
        $format = self::datetimeFormat($mysql);

        return gmdate($format, strtotime("+1 day", time()));
    }

    static function thirtyMinutesIntoTheFuture($mysql = false)
    {
        $format = self::datetimeFormat($mysql);

        return gmdate($format, strtotime("+30 minutes", time()));
    }

    /**
     * Returns the format for a datetime
     *
     * @param bool $mysql if true returns MySQL format if false PayPal's format
     * @return string
     */
    static function datetimeFormat($mysql = false)
    {
        $paypalFormat = "Y-m-d\TH:i:s\Z";
        $mysqlFormat = 'Y-m-d H:i:s';

        if ($mysql) {
            return $mysqlFormat;
        }

        return $paypalFormat;
    }

    /**
     * Returns an array with all countries' names
     *
     * @return array
     */
    static function countries()
    {
        return ["Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"];
    }

    /**
     * Returns an associative array of the countries
     * with the key and the value being the name of the country
     *
     * @return array
     */
    static function countriesForSelect()
    {
        $countries = self::countries();

        $result = [];

        $result[''] = 'Choose country';

        foreach ($countries as $country) {
            $result[$country] = $country;
        }

        return $result;
    }

    static function formPlacementChoices()
    {
        return [
            'left bottom',
            'right bottom',
            'bottom left',
            'bottom center',
            'bottom right',
        ];
    }

    static function formPlacementChoicesForSelect()
    {
        $choices = self::formPlacementChoices();

        $result = [];

        $result[''] = 'Choose position';

        foreach ($choices as $choice) {
            $result[$choice] = $choice;
        }

        return $result;
    }

    static function unsetSome($input)
    {
        $keys = ['email', 'password', 'api_key', 'ip_address'];

        foreach ($keys as $value) {
            unset($input[$value]);
        }

        return $input;
    }

    /**
     * Get the current offset from GMT in hours
     *
     * @return float|int
     */
    static function gmtOffset()
    {
        $tz = timezone_open(config('app.timezone'));

        $dateTimeSofia = date_create("now", timezone_open(config('app.timezone')));

        return timezone_offset_get($tz, $dateTimeSofia) / 3600;
    }

    /**
     * Sets the range of dates between which the subscribed
     * user will search for submissions
     */
    static function setSubmissionsRangeForSubscribedUser()
    {
        session(['subscribed' => ['start' => self::sinceUnix(), 'end' => self::furthestDatetime()]]);
    }

    /**
     * Sets the range of dates between which the free user
     * will search for submissions
     *
     * @param $month
     */
    static function setSubmissionsRangeForFreePlanUser($month)
    {
        $monthStart = self::monthStart($month);
        $monthEnd = self::monthEnd($monthStart);

        session(['free' => [
            'start' => $monthStart,
            'end' => $monthEnd,
            'month' => $month,
        ]]);
    }

    /**
     * Given a month in the following format: YYYY-MM
     * it return the first day of the month
     *
     * @param $month
     * @return string
     */
    static function monthStart($month)
    {
        return $month . '-01 00:00:00';
    }

    /**
     * Given a datetime it return the last second of the month
     * as a new full datetime
     *
     * @param $datetime
     * @return false|string
     */
    static function monthEnd($datetime)
    {
        $time = strtotime($datetime);

        return date('Y-m-t', $time) . ' 23:59:59';
    }

    static function sinceUnix()
    {
        return '1970-01-01 00:00:00';
    }

    static function furthestDatetime()
    {
        return '9999-01-01 00:00:00';
    }

    static function notifyUserOfLowEntriesLeft(User $user)
    {
        if ( ! $user->isSubscribed() AND ! $user->isOnGracePeriod() AND $user->notify_of_low_entries === 1) {
            $left = config('app.notify_if_left');
            $limit = config('plans.free.limit');

            $month = date('Y-m');
            $monthStart = Helper::monthStart($month);
            $monthEnd = Helper::monthEnd($monthStart);

            $submissions = DB::table('submissions')
                ->where('user_id', $user->id)
                ->where('created_at', '>=', $monthStart)
                ->where('created_at', '<=', $monthEnd)
                ->count();

            if ($submissions === $limit - $left) {
                SendNotifyUserOfLowEntriesLeftMailJob::dispatch($user);
            }
        }
    }

    static function notifyUserOfNewSubmission(User $user, Submission $submission)
    {
        if ($user->notify_on_new_submissions === 1) {
            SendNewSubmissionMailJob::dispatch($user, $submission);
        }
    }

    static function summerTime($datetime)
    {
        $date = new DateTime($datetime . ' ' . config('app.timezone'));

        if ($date->format('I') == 1) {
            return true;
        }

        return false;
    }
}