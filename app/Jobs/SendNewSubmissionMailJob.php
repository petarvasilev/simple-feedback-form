<?php

namespace App\Jobs;

use App\Mail\NewSubmissionMail;
use App\Submission;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendNewSubmissionMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user, $submission;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param Submission $submission
     */
    public function __construct(User $user, Submission $submission)
    {
        $this->user = $user;
        $this->submission = $submission;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->user)->send(new NewSubmissionMail($this->submission));
    }
}
