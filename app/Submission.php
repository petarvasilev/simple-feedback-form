<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    protected $fillable = ['ip_address', 'user_id', 'name', 'email', 'message', 'created_at_tz'];
}
