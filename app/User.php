<?php

namespace App;

use App\Jobs\SendAdminUserSubscribedNotificationMail;
use App\Jobs\SendSubscriptionActivatedMailJob;
use App\Jobs\SendSubscriptionDeactivatedMailJob;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'api_key', 'ip_address',
        'country',
        'address',
        'city',
        'post_code',
        'position',
        'background_color',
        'form_background_color',
        'paypal_subscription_starts_at',
        'subscription_activated_at',
        'notify_of_low_entries',
        'notify_on_new_submissions'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Activate subscription and optionally send email
     * to notify user
     *
     * @param bool $sendEmail
     */
    public function activateSubscription($sendEmail = true)
    {
        $this->subscription_status = 1;
        $this->subscription_activated_at = Carbon::now()->format('Y-m-d H:i:s');
        $this->save();

        if ($sendEmail) {
            SendSubscriptionActivatedMailJob::dispatch($this);
        }

        SendAdminUserSubscribedNotificationMail::dispatch($this);
    }

    /**
     * Deactivate a subscription and optionally send
     * email to notify user
     *
     * @param bool $sendEmail
     */
    public function deactivateSubscription($sendEmail = true)
    {
        $this->subscription_status = 0;
        $this->save();

        if ($sendEmail) {
            SendSubscriptionDeactivatedMailJob::dispatch($this);
        }
    }

    public function isSubscribed()
    {
        if ($this->subscription_status === 1) {
            return true;
        }

        return false;
    }

    public function isOnGracePeriod()
    {
        $found = User::where('id', $this->id)->where('grace_period_until', '>=', Carbon::now()->format('Y-m-d H:i:s'))->first();

        if ($found) {
            return $found->grace_period_until;
        }

        return false;
    }

    public function extendGracePeriod()
    {
        // month from now
        $datetime = Carbon::now()->addMonth(1)->format('Y-m-d H:i:s');

        if (config('services.paypal.mode') === 'sandbox') {
            // a day from now
            $datetime = Carbon::now()->addDay(1)->format('Y-m-d H:i:s');
        }

        $this->grace_period_until = $datetime;
        $this->save();
    }

    /**
     * Create a single string of the billing address
     *
     * @param bool $withName
     * @return string
     */
    public function getBillingAddress($withName = true)
    {
        $address = '';

        if ($withName) {
            $address .= $this->name;
        }

        if (trim($this->address) != '') {
            if ($withName) {
                $address .= ', ' . $this->address;
            } else {
                $address .= $this->address;
            }
        }

        if (trim($this->city) != '') {
            $address .= ', ' . $this->city;
        }

        if (trim($this->post_code) != '') {
            $address .= ' ' . $this->post_code;
        }

        if (trim($this->country) != '') {
            $address .= ', ' . $this->country;
        }

        return $address;
    }

    public function subscribedWithin15Minutes()
    {
        $timeSubscribed = strtotime($this->subscription_activated_at);
        $timeNow = time();
        $fifteenMinutes = 60*15;

        if ($timeSubscribed + $fifteenMinutes > $timeNow) {
            return true;
        }

        return false;
    }

    public function invoices()
    {
        return $this->hasMany('App\Invoice');
    }
}
