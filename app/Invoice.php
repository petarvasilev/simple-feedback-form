<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'user_id',
        'agreement_id',
        'amount',
        'amount_currency',
        'paypal_fee_amount',
        'paypal_fee_currency'
    ];
}
