<?php

namespace Tests\Unit;

use App\Logic\Helper;
use Tests\TestCase;

class SummerTimeTest extends TestCase
{
    public function testSummerTime()
    {
        $this->assertTrue(Helper::summerTime('2020-03-29 04:00:00'));
        $this->assertTrue(Helper::summerTime('2020-03-29 03:00:00'));
        $this->assertTrue(Helper::summerTime('2020-05-29 03:00:00'));
        $this->assertTrue(Helper::summerTime('2020-06-29 03:00:00'));
        $this->assertTrue(Helper::summerTime('2020-08-29 03:00:00'));
        $this->assertTrue(Helper::summerTime('2020-09-29 03:00:00'));
        $this->assertTrue(Helper::summerTime('2020-10-25 02:59:59'));
        $this->assertFalse(Helper::summerTime('2020-10-25 03:00:00'));
        $this->assertFalse(Helper::summerTime('2020-03-29 02:59:59'));
        $this->assertFalse(Helper::summerTime('2020-10-25 04:00:00'));
        $this->assertTrue(Helper::summerTime('2020-10-25 02:59:59'));

        $this->assertTrue(Helper::summerTime('2021-03-28 04:00:00'));
        $this->assertTrue(Helper::summerTime('2021-03-29 03:00:00'));
        $this->assertTrue(Helper::summerTime('2021-05-29 03:00:00'));
        $this->assertTrue(Helper::summerTime('2021-08-15 23:00:00'));
        $this->assertTrue(Helper::summerTime('2021-07-5 16:00:00'));
        $this->assertFalse(Helper::summerTime('2021-03-28 02:59:59'));
        $this->assertTrue(Helper::summerTime('2021-10-31 02:59:59'));
        $this->assertFalse(Helper::summerTime('2021-10-31 03:00:00'));
        $this->assertFalse(Helper::summerTime('2021-10-31 04:00:00'));
    }
}
