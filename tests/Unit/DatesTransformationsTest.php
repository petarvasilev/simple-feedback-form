<?php

namespace Tests\Unit;

use App\Logic\Helper;
use PHPUnit\Framework\TestCase;

class DatesTransformationsTest extends TestCase
{
    public function testMonthStart()
    {
        $this->assertEquals('2020-01-01 00:00:00', Helper::monthStart('2020-01'));
        $this->assertEquals('2020-02-01 00:00:00', Helper::monthStart('2020-02'));
        $this->assertEquals('2020-03-01 00:00:00', Helper::monthStart('2020-03'));
        $this->assertEquals('2020-04-01 00:00:00', Helper::monthStart('2020-04'));
        $this->assertEquals('2020-05-01 00:00:00', Helper::monthStart('2020-05'));
        $this->assertEquals('2020-06-01 00:00:00', Helper::monthStart('2020-06'));
        $this->assertEquals('2020-07-01 00:00:00', Helper::monthStart('2020-07'));
        $this->assertEquals('2020-08-01 00:00:00', Helper::monthStart('2020-08'));
        $this->assertEquals('2020-09-01 00:00:00', Helper::monthStart('2020-09'));
        $this->assertEquals('2020-10-01 00:00:00', Helper::monthStart('2020-10'));
        $this->assertEquals('2020-11-01 00:00:00', Helper::monthStart('2020-11'));
        $this->assertEquals('2020-12-01 00:00:00', Helper::monthStart('2020-12'));
        $this->assertEquals('2021-01-01 00:00:00', Helper::monthStart('2021-01'));
        $this->assertEquals('2021-02-01 00:00:00', Helper::monthStart('2021-02'));
    }

    public function testMonthEnd()
    {
        $this->assertEquals('2020-01-31 23:59:59', Helper::monthEnd('2020-01-01 00:00:00'));
        $this->assertEquals('2020-02-29 23:59:59', Helper::monthEnd('2020-02-15 15:45:12'));
        $this->assertEquals('2020-03-31 23:59:59', Helper::monthEnd('2020-03-03 01:45:12'));
        $this->assertEquals('2020-04-30 23:59:59', Helper::monthEnd('2020-04-30 23:59:59'));
        $this->assertEquals('2021-05-31 23:59:59', Helper::monthEnd('2021-05-10 00:59:59'));
        $this->assertEquals('2021-06-30 23:59:59', Helper::monthEnd('2021-06-29 17:59:59'));
        $this->assertEquals('2021-07-31 23:59:59', Helper::monthEnd('2021-07-29 17:59:59'));
        $this->assertEquals('2021-11-30 23:59:59', Helper::monthEnd('2021-11-29 17:59:59'));
        $this->assertEquals('2021-12-31 23:59:59', Helper::monthEnd('2021-12-29 17:59:59'));
        $this->assertEquals('2022-01-31 23:59:59', Helper::monthEnd('2022-01-29 17:59:59'));
        $this->assertEquals('2018-01-31 23:59:59', Helper::monthEnd('2018-01-29 17:59:59'));
        $this->assertEquals('2018-02-28 23:59:59', Helper::monthEnd('2018-02-25 17:59:59'));
    }
}
